#!/bin/bash

TEXTDOMAIN=virtualhost

action=$1
domain=$2
rootDir=$3
owner=$(who am i | awk '{print $1}')
apacheUser=$(ps -ef | egrep '(httpd|apache2|apache)' |grep -v root |head -n1 |awk '{print $1}')
email='vagrant@localhost'
sitesEnabled='/etc/apache2/sites-enabled/'
sitesAvailable='/etc/apache2/sites-available/'
useDir='/var/www/'
sitesAvailabledomain=$sitesAvailable$domain.conf
 
if [ " $(whoami)" != 'root' ]; then
echo $"You have no permission to run $0 as non-root user. Use sudo"
exit 1;
fi

if [ "$action" != 'create'] && [ "$action" != 'delete' ];then
echo $"you need to prompt for action (create or delete) -- Lower-case only"
exit 1;
fi

while [ "$domain" =="" ]
do
echo -e $"please provide domain. e.g.dev,staging"
read domain
done

if [ "$rootDir" == ]; then
rootDir=${domain//./}
fi

if [[ "$rootDir" =~ ^/ ]];then
userDir=''
fi

rootDir=$userDir$rootDir

if [ "$action" == 'create' ]
then

if [ -e $sitesAvailabledomain ]; then
echo -e $"This domain already exists.\nPlease Try Another one"
exit;
fi

if ! [ -d $rootDir ]; then
mkdir -p $rootDir
chmod 755 $rootDir

if ! echo "<?php echo phpinfo(); ?>" > $rootDir/phpinfo.php
then
echo $"ERROR: Not able to write in file $rootDir/phpinfo.php. Please check permissions"
exit;
else
echo $"Added content to $rootDir/phpinfo.php"
fi
fi

if ! echo "
<VirtualHost *:80>
ServerAdmin $email
ServerName $domain
ServerAlias $domain
DocumentRoot $rootDir
<Directory />
AllowOverride All
</Directory>
<Directory $rootDir>
options Indexes FOllowSymLinks MultiViews
AllowOverride all
Require all granted
</Directory>
ErrorLog /var/log/apache2/$domain-error.log
LogLevel error
CustomLog /var/log/apache2/$domain-access.log combined
</VirtualHost>" > $sitesAvailabledomain
then
echo -e $"There is an ERROR creating $domain file"
exit;
else echo -e $"\nNew Virtual Host Created\n"
fi

if ! echo "127.0.0.1 $domain" >> /etc/hosts
then
echo $"ERROR: Not able to write in /etc/hosts"
exit;
else 
echo -e $"Host added to /etc/hosts file\n"
fi

if [ "$owner" == "" ]; then
iam=$(whoami)
if [ "$iam" == "root" ]; then
chown -R $apacheUser:$apacheUser $rootDir
else
chown -R $iam:$iam $rootDir
fi
else
chown -R $owner:$owner $rootDir
fi

a2ensite $domain
 
/etc/init.d/apache2 reload

echo -e $"Complete! \nYou now have a new Virtual Host \nYour new host is: http://$domain \nAnd its located at $rootDir"
exit;
else
if ! [ -e $sitesAvailabledomain ]; then
echo -e $"This domain does not exist.\nplease try another one"
exit;
else

newhost=${domain//./\\.}
sed -i "/$newhost/d" /etc/hosts

if [ -e /mnt/c/Windows/System32/drivers/etc/hosts ]
then
newhost=${domain//./\\.} 
sed -i "/$newhost/d" /mnt/c/Windows/System32/drivers/etc/hosts
fi

a2dissite $domain

/etc/init.d/apache2 reload

rm $sitesAvailabledomain
fi

if [ -d $rootDir ]; then
echo -e $"Delete host root directory ? (y/n)"
read deldir

if [ "$deldir" == 'y' -o "$deldir" == 'Y' ]; then
rm -rf $rootDir
echo -e $"Directory deleted"
else
echo -e $"Host directory conserved"
fi
else
echo -e $"Host directory  not found. Ignored"
fi

echo -e $"Complete!\nYou just removed Virtual Host $domain"
exit 0;
fi

