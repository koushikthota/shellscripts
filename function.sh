#!/bin/bash

TITLE="System Information for $HOSTNAME"
RIGHT_NOW=$(date +"%x %r %z")
TIME_STAMP="Updated on $RIGHT_NOW by $USER"

show_uptime()

{

echo "<h2>system uptime</h2>"
echo "<pre>"
uptime
echo "</pre>"
}
function drive_space
{
echo "<h2>Filesystem space</h2>"
echo "<pre>"
df -h
echo "</pre>"
}

function home_space
{
echo "<h2>home directory space by user</h2>"
echo "<pre>"
echo "bytes directory"
du -s /home/* | sort -nr
echo "</pre>"
}

cat <<-_EOF_
<html>
<head>
<title>$TITLE</title
</head>
<body>
<h1>$TITLE</h1>
<p>$TIME_STAMP</p>
$(show_uptime)
$(drive_space)
$(home_space)
</body>
</html>
_EOF_
